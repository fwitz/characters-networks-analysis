# Characters Networks Analysis

This project was realized as a final research project for the MR52 course at UTBM. By using Mathieu Triclot's and Yannick Rochat's method, we aim to study the characters' networks of the SF saga 'Dune' to analyse the building of the universe and the relationships between characters in the story using graphs.

So, the first part of the project is the generation of those graphs using R scripts and their analysis and the second one is the contribution to the method from technical point of view.
