import sys
import os

import csv
from matplotlib import pyplot
import matplotlib as plt

from collections import Counter

def attr_proportion(attribute_file: str):
    """ Method that produce a donut vizualisation to represent the proportion of a particular attribute in a book"""
    with open(attribute_file, 'r') as file:
        csv_file = csv.reader(file, delimiter=",")
        header = next(csv_file) # Skip header
        # Retrieve attributes list
        attr_list = []
        for row in csv_file:
            attr_list.append(row[1])
        c = Counter(attr_list) # Counting every attribute
        x, y = [], []
        for k,v in c.items():
            x.append(k.strip())
            y.append(v)
        # Plotting pie chart
        f = pyplot.figure(figsize = (8, 8))
        pyplot.get_current_fig_manager().set_window_title(os.path.splitext(attribute_file.split('/')[-1])[0][:-5] + ".attr-proportion")
        pyplot.pie(
            y, 
            rotatelabels=True,
            pctdistance = 0.8, 
            normalize = True,
            colors=pyplot.colormaps['tab20'].colors
        )
        # add a circle at the center to transform it in a donut chart
        center_cicle=pyplot.Circle( (0,0), 0.6, color='white')
        p = pyplot.gcf()
        p.gca().add_artist(center_cicle)
        pyplot.legend(loc='lower center', bbox_to_anchor=(0.5, -0.15), labels = x, ncol=2)
        pyplot.style.use('ggplot')
        pyplot.show()

def attr_evolution(attribute_files: list[str]):
    """ Method that, given a list of attribute files, give in output the evolution of those attributes in the files"""
    counter_list = []
    for f in attribute_files:
        with open(f, 'r') as file:
            csv_file = csv.reader(file, delimiter=',')
            header = next(csv_file)
            attr_list = []
            for row in csv_file:
                attr_list.append(row[1])
            # Counting every attribute and adding it in the counter list
            counter_list.append(Counter(attr_list))
    # Getting all the unique attributes
    attr = sorted(set(sum([[e.strip() for e in list(c.keys())] for c in counter_list], [])))
    # Grouping all the attributes
    data = {}
    for a in attr:
        data[a] = []
        for c in counter_list:
            if a in list(c.keys()):
                for i in list(c.items()):
                    if i[0] == a:
                        data[a].append(i[1])
            else:
                data[a].append(0)
    # Plotting all curves
    palette = pyplot.get_cmap('tab20')
    num=0
    x = range(1, len(counter_list)+1)
    for key,value in data.items():
        num+=1
        # Plot every group, but discrete
        for k,v in data.items():
            pyplot.plot(x, v, marker='', color='grey', linewidth=0.6, alpha=0.3)
        pyplot.plot(x, value, marker='', linewidth=2.4, alpha=0.9, label=key, color=palette(num))
        pyplot.title(key, loc='center', fontsize=12, fontweight=0)
        pyplot.style.use('ggplot')
        pyplot.xlabel('Tome')
        pyplot.ylabel('Nombre de personnage')
        pyplot.get_current_fig_manager().set_window_title(key + ".attr.evolution")
        pyplot.show()
    #Plotting area curves
    y = [v for k,v in data.items()]
    pyplot.stackplot(x, y, labels=attr, colors=pyplot.colormaps['tab20'].colors)
    pyplot.xlabel('Tome')
    pyplot.ylabel('Nombre de personnage')
    pyplot.legend(loc='upper left')
    pyplot.style.use('ggplot')
    pyplot.get_current_fig_manager().set_window_title("Global.attr.evolution")
    pyplot.show()

def attr_number(attribute_files: list[str]):
    """ Method that given a list of attribute files give in output a graph describing the evolution of attributes """
    counter_list = []
    for f in attribute_files:
        with open(f, 'r') as file:
            csv_file = csv.reader(file, delimiter=',')
            header = next(csv_file)
            attr_list = []
            for row in csv_file:
                attr_list.append(row[1])
            # Counting every attribute and adding it in the counter list
            counter_list.append(Counter(attr_list))
    # Getting number of attributes per file
    attr_nb = [len(list(set([e.strip() for e in list(c.keys())]))) for c in counter_list]
    x = range(1, len(counter_list)+1)
    pyplot.style.use('ggplot')
    pyplot.plot(x, attr_nb, marker='', linewidth=2.4, alpha=0.9)
    pyplot.xlabel('Tome')
    pyplot.ylabel('Nombre de factions')
    pyplot.get_current_fig_manager().set_window_title("attr.number.evolution")
    pyplot.show()

if __name__ == "__main__":
    print("Beginning analysis...")
    func = sys.argv[1]
    path = sys.argv[2:]
    can_go = True
    for p in path:
        if not os.path.exists(p):
            can_go = False
    if can_go:
        if func == "-p":
            attr_proportion(path[0])
        elif func == "-e":
            attr_evolution(path)
        elif func == '-n':
            attr_number(path)
        print("Work done !")
    else:
        print("One of the files doesn't exists or on of the paths is incorrect !")