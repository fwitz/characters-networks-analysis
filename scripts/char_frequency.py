import sys
import os

import csv
from matplotlib import pyplot

def nb_evolution(attribute_files: str):
    """ Method that given a set of attributes files """
    nb_list = []
    for f in attribute_files:
        with open(f, 'r') as file:
            csv_file = csv.reader(file, delimiter=',')
            header = next(csv_file)
            nb_list.append(len([row for row in csv_file]))
    x = range(1, len(nb_list)+1)
    pyplot.style.use('ggplot')
    pyplot.plot(x, nb_list)
    pyplot.xlabel('Tome')
    pyplot.ylabel('Nombre de personnage')
    pyplot.get_current_fig_manager().set_window_title("char.number.evolution")
    pyplot.show()

if __name__ == "__main__":
    print("Beginning analysis...")
    func = sys.argv[1]
    path = sys.argv[2:]
    can_go = True
    for p in path:
        if not os.path.exists(p):
            can_go = False
    if can_go:
        if func == "-e":
            nb_evolution(path)
        print("Work done !")
    else:
        print("One of the files doesn't exists or on of the paths is incorrect !")