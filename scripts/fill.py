import os
from posixpath import dirname
import sys
import csv
import ntpath

def fill(file_path: str) -> None:
    """ Method that take a character csv file and fill the blank with zeroes """
    # Creating a temporary file in the file directory to make the changes
    tmp = os.path.join(dirname(file_path), "tmp.csv")
    # Opening the two files
    with open(file_path, "r") as input_file, open(tmp, "w") as out_file:
        # Reading them using the csv reader
        reader = csv.reader(input_file, delimiter=',')
        writer = csv.writer(out_file, delimiter=',')
        # Keeping intact the head row
        header = next(reader)
        writer.writerow(header)
        # Then we go through each row and fill with a 0 if there is no value
        # and we keep the value if there is one
        for row in reader:
            col_values = []
            for col in row:
                if col == '':
                    col_values.append(0)
                else:
                    col_values.append(col)
            writer.writerow(col_values) # Write the modified line to the tmp file
    os.remove(file_path) # Remove the original file
    # Rename the temporary file as the original file
    os.rename(tmp, os.path.join(dirname(file_path), ntpath.basename(file_path)))


if __name__ == "__main__":
    path = sys.argv[1]
    if os.path.exists(path):
        print("Beginning filling zeroes...")
        fill(path)
        print("Work done !")
    else:
        print("The file doesn't exists or the path is incorrect !")